package Entidad;

import ufps.util.colecciones_seed.Pila;

public class CentroVotacion {

    private int id_centro;

    private String nombreCentro;

    private String direccion;
    
    private Pila<Mesa> mesas=new Pila();

    public CentroVotacion() {
    }

    public CentroVotacion(int id_centro, String nombreCentro, String direccion) {
        this.id_centro = id_centro;
        this.nombreCentro = nombreCentro;
        this.direccion = direccion;
    }

    public int getId_centro() {
        return id_centro;
    }

    public void setId_centro(int id_centro) {
        this.id_centro = id_centro;
    }

    public String getNombreCentro() {
        return nombreCentro;
    }

    public void setNombreCentro(String nombreCentro) {
        this.nombreCentro = nombreCentro;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public Pila<Mesa> getMesas() {
        return mesas;
    }

    public void setMesas(Pila<Mesa> mesas) {
        this.mesas = mesas;
    }
    
    
    
}
